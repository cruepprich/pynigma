# PyNigma

Plan for building an Enigma machine emulator in Python.

[General Description](http://enigma.louisedade.co.uk/enigma.html)

[Flow diagram](http://bit.ly/enigma_pic)

This is a command line driven program. As the Enigma machine is very complex, we will progress in small steps from easy to hard. This will help to teach Python and programming priciples.

There will be multiple phases, each building on top of the previous.

## Phase 1

Program: phase1.py

Create a program that encodes/decodes a single letter between A and C. It accepts a command line option (-m) _mode_ followed by the mode _encode_ or _decode_. The letter to encode/decode will also be passed in the command line.

### Example

```bash
# python phase1.py -m encode A
# C
# python phase1.py -m decode C
# A
```