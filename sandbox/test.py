"""
Fun is a class that manipulates a list. 
The inc method simply appends an element to the list.
The reset method should reset the list (self.lst) to its original state
from when the instance was created. So if the instance was created with
[1,2], then inc was run, making the list [1,2,3]. The reset method should
reset the list to [1,2]

The test runs below show the following:
Test 1a, works as expected. 
When inc() is run, the list is correctly appended:
    copy_ref changes to [1,2,3] when inc is invoked. This is expected
    because copy_ref was copied by reference. copy_val remains [1,2]
When reset() is run, val is correctly reset to [1,2], but copy_ref
remains [1,2,3]. I expected it to also be set to [1,2]

Now I run the test a second time:
Test 1b behaves wierdly: When inc() is run, val, copy_val, and copy_ref
are all set to [1,2,3]. How is it possible for copy_val to change? It was
copied by value, and should always remain [1,2].
The reset() method is inaffective. None of the variables are set back
to [1,2]


For tests 2a and 2b, I recreate the fun_inst instance.
Test 1a works as expected. In this test reset2() is used. It explicitly
assigns all three variables 
"""
import copy
class Fun():
    def __init__(self, lst):
        self.lst      = lst
        self.copy_val = lst[:] #copy by lstue
        self.copy_ref = lst    #copy by reference

    def inc(self):
        # expect lst and copy_ref = [1,2,3]
        self.lst.append(3)

    def reset(self):
        # expect lst and copy_ref = [1,2]
        self.lst = copy.deepcopy(self.copy_val)
        # self.lst = self.copy_val 

    def reset2(self):
        # this one works
        # expect lst and copy_ref = [1,2]
        self.lst = self.copy_val
        self.copy_ref = self.copy_val
        self.copy_val = self.copy_val[:]




fun_inst = Fun([1,2])
print('Test 1a')
print('Initial:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.inc()
print('Increase:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.reset()
print('Reset:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)


# fun_inst = Fun([1,2])
print('\n\nTest 1b')
print('Initial:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.inc()
print('Increase:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.reset()
print('Reset:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)



fun_inst = Fun([1,2])
print('\n\n\nTest 2a')
print('Initial:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.inc()
print('Increase:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.reset2()
print('Reset:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)


# fun_inst = Fun([1,2])
print('\n\nTest 2b')
print('Initial:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.inc()
print('Increase:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)
fun_inst.reset2()
print('Reset:',fun_inst.lst,fun_inst.copy_ref,fun_inst.copy_val)




