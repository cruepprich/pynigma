import random as rnd
import itertools as it
# bit.ly/enigma_pic
# rnd_letters = list(map(lambda x: chr(x), rnd.sample(range(65,91),25)))
CIPHERS = [
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
    ['H','F','X','O','L','S','B','P','Y','D','G','I','A','T','J','K','Z','C','Q','V','W','N','U','M','R','E'],
    ['R','F','A','N','S','W','G','U','P','X','Q','T','I','K','B','H','V','E','C','M','L','O','Y','J','D','Z'],
    ['I','M','A','L','V','S','D','Y','W','T','O','H','Z','U','F','E','B','X','G','K','P','J','C','R','N','Q']
]

ROTORS = [
    {"name":"I",	"value":"EKMFLGDQVZNTOWYHXUSPAIBRCJ"},
    {"name":"II",	"value":"AJDKSIRUXBLHWTMCQGZNPYFVOE"},
    {"name":"III",	"value":"BDFHJLCPRTXVZNYEIWGAKMUSQO"},
    {"name":"IV",	"value":"ESOVPZJAYQUIRHXLNFTGKDCMWB"},
    {"name":"V",	"value":"VZBRGITYUPSDNHLXAWMJQOFECK"},
    {"name":"VI",	"value":"JPGVOUMFYQBENHZRDKASXLICTW"},
    {"name":"ETW",	"value":"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
    {"name":"Reflector A", "value":"EJMZALYXVBWFCRQUONTSPIKHGD"},		
    {"name":"Reflector B", "value":"YRUHQSLDPXNGOKMIEBFZCWVJAT"},		
    {"name":"Reflector C", "value":"FVPJIAOYEDRZXWGCTKUQSBNMHL"}
]

def get_rotor_cipher(name):
    try:
        return list(filter(lambda x: x["name"]== name ,ROTORS))[0]["value"]
    except IndexError:
        print("Invalid rotor name", name)
        exit(2)

class Cipher:
    def __init__(self, cipher):
        self.cipher           = cipher #List of 26 uppercase letters in random order
        self.ORIGINAL_CIPHER  = cipher #List of 26 uppercase letters in random order
        self.cipher_length    = len(self.cipher) #Length of letters list
        

class Rotor(Cipher):
    def __init__(self, cipher):
        return super().__init__(cipher)
        self.rotation_index = 0
        self.alphabet       = CIPHERS[0]

    # def __init__(self,letter_list): 

        
    def show(self):
        print("Cipher:\n{}\nLength: {}".format(self.cipher,self.cipher_length))

    def show_rotation_index(self):
        print(self.rotation_index)

    

    def reset_rotor(self):
        # resets the rotor to the original position
        self.cipher = self.ORIGINAL_CIPHER

    def rotate(self, show=False):
        # Advances rotor by 1
        # increase index by 1, if list length is exceeded go back to 0
        self.rotation_index += 1
        if self.rotation_index > self.cipher_length -1:
            self.rotation_index = self.rotation_index - self.cipher_length

        new_letters = []
        # create cycle to cycle through letters
        it_letters  = it.cycle(self.cipher)
        next(it_letters) # skip the first element

        # construct new array beginning at the second element
        new_letters = list(map(lambda x: next(it_letters),range(self.cipher_length)))


        self.cipher = list(new_letters)

        if show:
            print(self.cipher)


    def set_rotation_index(self,idx):
        # Rotates the rotor idx times from the original position
        self.reset_rotor()
        if idx not in range(self.cipher_length):
            print("ERROR: Rotation index must be between 0 and {}".format(self.cipher_length-1))
            exit(2)

        for rotate in range(idx):
            self.rotate()


    def encode(self, text):
        
        text      = text.replace(' ','Z') # Replace spaces with Z (least used character in English)
        text_list = list(text)            # Convert string to list
        text_out  = []                    # initialize output list
        self.reset_rotor()                # We'll change that to a specific position later

        for letter in text_list:
            # Index of this letter in the alphabet
            alpha_idx = self.alphabet.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.cipher[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        return ''.join(text_out)

    def decode(self, text):
        text_list = list(text)
        text_out  = []
        self.reset_rotor()

        for letter in text_list:
            # Index of this letter in the rotor
            alpha_idx = self.cipher.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.alphabet[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        # Replace first consecutive Z with space
        decoded_text = ''.join(text_out)
        return decoded_text
        
        

    


rotor_a = Rotor(get_rotor_cipher('Reflector A'))



encrypted_msg = rotor_a.encode('AAAA')
print(encrypted_msg)
decrypted_msg = rotor_a.decode(encrypted_msg)
print(decrypted_msg.replace('Z',' '))

