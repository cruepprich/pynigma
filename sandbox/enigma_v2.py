import itertools as it


class Reflector:
    '''
    The reflector class and en/decode a string based on the cipher it gets from the rotor_name.
    '''

    def __init__(self, rotor_name):
        self.ROTORS = [
            {"name":"I",	"value":"EKMFLGDQVZNTOWYHXUSPAIBRCJ"},
            {"name":"II",	"value":"AJDKSIRUXBLHWTMCQGZNPYFVOE"},
            {"name":"III",	"value":"BDFHJLCPRTXVZNYEIWGAKMUSQO"},
            {"name":"IV",	"value":"ESOVPZJAYQUIRHXLNFTGKDCMWB"},
            {"name":"V",	"value":"VZBRGITYUPSDNHLXAWMJQOFECK"},
            {"name":"VI",	"value":"JPGVOUMFYQBENHZRDKASXLICTW"},
            {"name":"ETW",	"value":"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
            {"name":"Reflector A", "value":"EJMZALYXVBWFCRQUONTSPIKHGD"},		
            {"name":"Reflector B", "value":"YRUHQSLDPXNGOKMIEBFZCWVJAT"},		
            {"name":"Reflector C", "value":"FVPJIAOYEDRZXWGCTKUQSBNMHL"}
        ]
        self.cipher           = list(filter(lambda x: x["name"]== rotor_name ,self.ROTORS))[0]["value"]
        self.ORIGINAL_CIPHER  = self.cipher #List of 26 uppercase letters in random order
        self.CIPHER_LENGTH    = len(self.cipher) #Length of letters list
        self.ALPHABET         = list(filter(lambda x: x["name"]== 'ETW' ,self.ROTORS))[0]["value"]


    def show(self):
        print("Cipher:\n{}\nLength: {}".format(self.cipher,self.CIPHER_LENGTH))

    def show_cipher(self):
        print("Cipher:\n{}".format(self.cipher))


    def encode(self, text):
        
        text      = text.replace(' ','Z') # Replace spaces with Z (least used character in English)
        text_list = list(text)            # Convert string to list
        text_out  = []                    # initialize output list
        # self.reset_rotor()                # We'll change that to a specific position later

        for letter in text_list:
            # Index of this letter in the alphabet
            alpha_idx = self.ALPHABET.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.cipher[alpha_idx]
            text_out.append(new_letter)
            # self.rotate() #Advance rotor afer each letter

        return ''.join(text_out)

    def decode(self, text):
        text_list = list(text)
        text_out  = []
        # self.reset_rotor()

        for letter in text_list:
            # Index of this letter in the rotor
            alpha_idx = self.cipher.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.ALPHABET[alpha_idx]
            text_out.append(new_letter)
            # self.rotate() #Advance rotor afer each letter

        # Replace first consecutive Z with space
        decoded_text = ''.join(text_out)
        return decoded_text




class Rotor(Reflector):
    '''
    Rotor inherits Reflector, and adds methods for rotation.Rotor
    '''
    def __init__(self, rotor_name):
        self.rotation_index = 0
        return super().__init__(rotor_name)


    def show(self):
        print("Rotor:\n{}\nLength: {}\n{}".format(self.cipher,self.CIPHER_LENGTH,self.ALPHABET))

    def show_rotation_index(self):
        print(self.rotation_index)

    

    def reset_rotor(self):
        # resets the rotor to the original position
        self.cipher = self.ORIGINAL_CIPHER


    def set_rotation_index(self,idx):
        # Rotates the rotor idx times from the original position
        self.reset_rotor()

        for rotate in range(idx % self.CIPHER_LENGTH): #use modulo to continuously rotate rotor
            self.rotate()


    def rotate(self, show=False):
        # Advances rotor by 1
        # increase index by 1, if list length is exceeded go back to 0
        self.rotation_index += 1
        if self.rotation_index > self.CIPHER_LENGTH -1:
            self.rotation_index = self.rotation_index - self.CIPHER_LENGTH

        new_letters = []
        # create cycle to cycle through letters
        it_letters  = it.cycle(self.cipher)
        next(it_letters) # skip the first element

        # construct new array beginning at the second element
        new_letters = list(map(lambda x: next(it_letters),range(self.CIPHER_LENGTH)))


        self.cipher = list(new_letters)

        if show:
            print('New cipher: ',self.cipher)

    def reflector_encode(self,text):
        return super().encode(text)


    def reflector_decode(self,text):
        return super().decode(text)





    def rotor_encode(self, text, rotor_idx=0):
        '''
        After encoding each letter, the rotor advances by one.
        '''
        text      = text.replace(' ','Z') # Replace spaces with Z (least used character in English)
        text_list = list(text)            # Convert string to list
        text_out  = []                    # initialize output list
        self.reset_rotor()                # We'll change that to a specific position later
        self.set_rotation_index(rotor_idx)

        for letter in text_list:
            # Index of this letter in the alphabet
            alpha_idx = self.ALPHABET.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.cipher[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        return ''.join(text_out)





    def rotor_decode(self, text, rotor_idx=0):
        '''
        After decoding each letter, the rotor advances by one.
        '''
        text_list = list(text)
        text_out  = []
        self.reset_rotor()
        self.set_rotation_index(rotor_idx)

        for letter in text_list:
            # Index of this letter in the rotor
            alpha_idx = self.cipher.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.ALPHABET[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        # Replace first consecutive Z with space
        decoded_text = ''.join(text_out)
        return decoded_text




    # def encode(self, text, rotor_idx=0):
    #     msg = self.rotor_encode(text,rotor_idx)
    #     msg = self.reflector_encode(msg)
    #     return msg



    # def decode(self, text, rotor_idx=0):
    #     msg = self.reflector_decode(text)
    #     msg = self.rotor_decode(msg,rotor_idx)
    #     return msg




rotors = ['I','II','III']
rotor = Rotor('I')
rotation_index = 2


# for idx in range(3):
#     encoded = rotor.rotor_encode(msg,rotation_index)
#     print(encoded,rotor.rotor_decode(encoded,rotation_index))
#     rotation_index +=1

# ref_encode = rotor.rotor_encode(msg, rotation_index)
# print(ref_encode, rotor.rotor_decode(ref_encode, rotation_index))


# Enigma
reflector = Reflector('Reflector A')
rotor_a = Rotor('I')
rotor_b = Rotor('II')
rotor_c = Rotor('III')
msg = 'ABCDEFG'


# Ecode
for idx in range(100):

    rotation_index = idx
    encoded_msg = rotor_a.rotor_encode(msg,rotation_index)
    encoded_msg = rotor_b.rotor_encode(encoded_msg,rotation_index)
    encoded_msg = rotor_c.rotor_encode(encoded_msg,rotation_index)
    encoded_msg = reflector.encode(encoded_msg)
    encoded_msg = rotor_c.rotor_encode(encoded_msg,rotation_index)
    encoded_msg = rotor_b.rotor_encode(encoded_msg,rotation_index)
    encoded_msg = rotor_a.rotor_encode(encoded_msg,rotation_index)
    # if 'A' in encoded_msg:
    #     print('encoded:',encoded_msg)
        
    # print('encoded:',encoded_msg)

    decoded_msg = rotor_a.rotor_decode(encoded_msg,rotation_index)
    decoded_msg = rotor_b.rotor_decode(decoded_msg,rotation_index)
    decoded_msg = rotor_c.rotor_decode(decoded_msg,rotation_index)
    decoded_msg = reflector.decode(decoded_msg)
    decoded_msg = rotor_c.rotor_decode(decoded_msg,rotation_index)
    decoded_msg = rotor_b.rotor_decode(decoded_msg,rotation_index)
    decoded_msg = rotor_a.rotor_decode(decoded_msg,rotation_index)

    print('Decoded: ',encoded_msg,decoded_msg)
