import random as rnd
import itertools as it

ROTORS = [
    {"name":"I",	"value":"EKMFLGDQVZNTOWYHXUSPAIBRCJ"},
    {"name":"II",	"value":"AJDKSIRUXBLHWTMCQGZNPYFVOE"},
    {"name":"III",	"value":"BDFHJLCPRTXVZNYEIWGAKMUSQO"},
    {"name":"IV",	"value":"ESOVPZJAYQUIRHXLNFTGKDCMWB"},
    {"name":"V",	"value":"VZBRGITYUPSDNHLXAWMJQOFECK"},
    {"name":"VI",	"value":"JPGVOUMFYQBENHZRDKASXLICTW"},
    {"name":"ETW",	"value":"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
    {"name":"Reflector A", "value":"EJMZALYXVBWFCRQUONTSPIKHGD"},		
    {"name":"Reflector B", "value":"YRUHQSLDPXNGOKMIEBFZCWVJAT"},		
    {"name":"Reflector C", "value":"FVPJIAOYEDRZXWGCTKUQSBNMHL"}
]

class Cipher:
    def __init__(self, rotor_name):

        self.cipher           = list(filter(lambda x: x["name"]== rotor_name ,ROTORS))[0]["value"]
        self.ORIGINAL_CIPHER  = self.cipher #List of 26 uppercase letters in random order
        self.CIPHER_LENGTH    = len(self.cipher) #Length of letters list
        self.ALPHABET         = list(filter(lambda x: x["name"]== 'ETW' ,ROTORS))[0]["value"]

    def show(self):
        print("Cipher:\n{}\nLength: {}".format(self.cipher,self.CIPHER_LENGTH))

    def show_cipher(self):
        print("Cipher:\n{}".format(self.cipher))


    def encode(self, text):
        
        text      = text.replace(' ','Z') # Replace spaces with Z (least used character in English)
        text_list = list(text)            # Convert string to list
        text_out  = []                    # initialize output list
        # self.reset_rotor()                # We'll change that to a specific position later

        for letter in text_list:
            # Index of this letter in the alphabet
            alpha_idx = self.ALPHABET.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.cipher[alpha_idx]
            text_out.append(new_letter)
            # self.rotate() #Advance rotor afer each letter

        return ''.join(text_out)

    def decode(self, text):
        text_list = list(text)
        text_out  = []
        # self.reset_rotor()

        for letter in text_list:
            # Index of this letter in the rotor
            alpha_idx = self.cipher.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.ALPHABET[alpha_idx]
            text_out.append(new_letter)
            # self.rotate() #Advance rotor afer each letter

        # Replace first consecutive Z with space
        decoded_text = ''.join(text_out)
        return decoded_text




class Rotor(Cipher):

    def __init__(self, rotor_name):
        self.rotation_index = 0
        return super().__init__(rotor_name)


    def show(self):
        print("Rotor:\n{}\nLength: {}\n{}".format(self.cipher,self.CIPHER_LENGTH,self.ALPHABET))

    def show_rotation_index(self):
        print(self.rotation_index)

    

    def reset_rotor(self):
        # resets the rotor to the original position
        self.cipher = self.ORIGINAL_CIPHER


    def set_rotation_index(self,idx):
        # Rotates the rotor idx times from the original position
        self.reset_rotor()
        if idx not in range(self.cipher_length):
            print("ERROR: Rotation index must be between 0 and {}".format(self.cipher_length-1))
            exit(2)

        for rotate in range(idx):
            self.rotate()


    def rotate(self, show=False):
        # Advances rotor by 1
        # increase index by 1, if list length is exceeded go back to 0
        self.rotation_index += 1
        if self.rotation_index > self.CIPHER_LENGTH -1:
            self.rotation_index = self.rotation_index - self.CIPHER_LENGTH

        new_letters = []
        # create cycle to cycle through letters
        it_letters  = it.cycle(self.cipher)
        next(it_letters) # skip the first element

        # construct new array beginning at the second element
        new_letters = list(map(lambda x: next(it_letters),range(self.CIPHER_LENGTH)))


        self.cipher = list(new_letters)

        if show:
            print('New cipher: ',self.cipher)

    def encode(self, text):
        
        text      = text.replace(' ','Z') # Replace spaces with Z (least used character in English)
        text_list = list(text)            # Convert string to list
        text_out  = []                    # initialize output list
        self.reset_rotor()                # We'll change that to a specific position later

        for letter in text_list:
            # Index of this letter in the alphabet
            alpha_idx = self.ALPHABET.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.cipher[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        return ''.join(text_out)

    def decode(self, text):
        text_list = list(text)
        text_out  = []
        self.reset_rotor()

        for letter in text_list:
            # Index of this letter in the rotor
            alpha_idx = self.cipher.index(letter)
            # What is the letter of that index in the rotor?
            new_letter = self.ALPHABET[alpha_idx]
            text_out.append(new_letter)
            self.rotate() #Advance rotor afer each letter

        # Replace first consecutive Z with space
        decoded_text = ''.join(text_out)
        return decoded_text


reflector = Cipher('I')
encoded = reflector.encode('AAA')
print(encoded)
print(reflector.decode(encoded))


rotor = Rotor('I')

for idx in range(3):
    encoded = rotor.encode('AAA')
    print(encoded,rotor.decode(encoded))
    rotor.show_rotation_index()