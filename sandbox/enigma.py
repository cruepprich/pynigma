import itertools as it
import copy

"""
Generate reflector:
ref = zip(list(map(lambda x: chr(x), rnd.sample(range(65,78),13)))
         ,list(map(lambda x: chr(x), rnd.sample(range(78,91),13))))
numeric reflector: 
ref = list(zip(list(map(lambda x: x, rnd.sample(range(0,13),13))) ,list(map(lambda x: x, rnd.sample(range(13,26),13)))))


Generate rotor:
rndlist = list(map(lambda x: chr(x), rnd.sample(range(65,91),26)))
rotor = list((chr(idx + 65),value) for idx, value in enumerate(rndlist))

numeric rotor:
rndlist = list(map(lambda x: x, rnd.sample(range(0,26),26)))
print(list((idx,value) for idx, value in enumerate(rndlist)))

"""
ROTORS = [
    {"name":"I",    "value":[18, 15, 11, 21, 13, 8, 6, 4, 10, 1, 25, 17, 19, 20, 22, 16, 0, 5, 3, 14, 23, 7, 24, 9, 12, 2]},
    {"name":"II",   "value":[7, 14, 24, 12, 21, 25, 10, 20, 23, 9, 16, 4, 15, 0, 3, 1, 22, 17, 6, 11, 13, 5, 8, 18, 19, 2]},
    {"name":"III",  "value":[19, 14, 4, 25, 17, 20, 23, 3, 13, 1, 10, 16, 8, 21, 2, 22, 7, 6, 9, 12, 18, 0, 15, 24, 11, 5]},
    {"name":"IV",   "value":[13, 4, 22, 12, 7, 14, 1, 18, 0, 10, 9, 24, 16, 23, 19, 11, 3, 8, 6, 21, 5, 15, 20, 25, 17, 2]},
    {"name":"V",    "value":[11, 8, 20, 10, 3, 13, 22, 0, 9, 15, 23, 5, 25, 16, 17, 18, 2, 19, 12, 24, 7, 1, 14, 21, 4, 6]},
]
REFLECTORS = [
    {"name":"A", "value":[('M', 'N'), ('A', 'O'), ('K', 'P'), ('D', 'W'), ('C', 'Y'), ('E', 'X'), ('G', 'V'), ('F', 'Z'), ('J', 'R'), ('H', 'S'), ('L', 'T'), ('B', 'Q'), ('I', 'U')]},
    {"name":"N", "value":[(1, 22), (5, 17), (10, 13), (2, 20), (6, 18), (12, 21), (4, 16), (0, 15), (9, 14), (8, 24), (11, 19), (3, 25), (7, 23)]}
]





class Rotor():
    '''
    Rotor 
    '''
    def __init__(self, rotor_idx):
        self.rotation_index = 0
        self.rotor          = rotor_idx["value"]
        self.ORIGINAL_ROTOR = rotor_idx["value"]#copy by value
        # self.ORIGINAL_ROTOR = copy.deepcopy(rotor_idx["value"])#copy by value
        # self.rotor          = list(filter(lambda x: x["name"]== rotor_name ,ROTORS))[0]["value"]
        self.ROTOR_LENGTH   = len(self.rotor)


    def show(self):
        print("Rotor:\n{}".format(self.rotor))

    def show_rotation_index(self):
        print(self.rotation_index)

    

    def reset(self,rotor_idx=0):
        # resets the rotor to the original position
        # print('reset a',self.rotor)
        self.rotation_index = 0
        self.rotor = copy.deepcopy(self.ORIGINAL_ROTOR)
        
        if rotor_idx > 0:
            for rotate in range(rotor_idx % self.ROTOR_LENGTH): #use modulo to continuously rotate rotor
                self.rotate()


    def set_rotation_index(self,idx):
        # Rotates the rotor idx times from the original position
        self.reset()

        for rotate in range(idx % self.ROTOR_LENGTH): #use modulo to continuously rotate rotor
            self.rotate()


    def rotate(self, show=False):
        # pop the last element and insert it in front
        self.rotor.insert(0,self.rotor.pop())

        self.rotation_index += 1
        if self.rotation_index > self.ROTOR_LENGTH -1:
            self.rotation_index = self.rotation_index - self.ROTOR_LENGTH

        # print('rotate',self.rotation_index,self.ORIGINAL_ROTOR)

            
    def encode_idx(self,idx):
        return self.rotor[idx]

    def decode_idx(self,idx):
        return self.rotor.index(idx)


rotor_a = Rotor(ROTORS[0])
rotor_b = Rotor(ROTORS[1])
rotor_c = Rotor(ROTORS[2])

def reset_rotors(rotor_idxs):
    rotor_a.reset(rotor_idxs[0])
    rotor_b.reset(rotor_idxs[1])
    rotor_c.reset(rotor_idxs[2])
    # print('Reset rotor indexes {} {} {}'.format(rotor_a.rotation_index,rotor_b.rotation_index,rotor_c.rotation_index))

def rotate_rotors():
    # Rotate the rotors like clockwork
    if rotor_a.rotation_index == 25:
        if rotor_b.rotation_index == 25:
            rotor_c.rotate()
            # print('rotor indexes {} {} {} {}'.format(rotor_a.rotation_index,rotor_b.rotation_index,rotor_c.rotation_index,rotor_c.rotor))

        rotor_b.rotate()
        
    rotor_a.rotate()

def encode_letter(letter):
    encoded_msg = rotor_a.encode(letter)
    encoded_msg = rotor_b.encode(encoded_msg)
    encoded_msg = rotor_c.encode(encoded_msg)
    encoded_msg = reflector.encode(encoded_msg)
    # print('encoded_msg',encoded_msg)
    return encoded_msg
    

def decode_letter(letter):
    decoded_msg = reflector.decode(letter)
    decoded_msg = rotor_c.decode(decoded_msg)
    decoded_msg = rotor_b.decode(decoded_msg)
    decoded_msg = rotor_a.decode(decoded_msg)
    # print('decoded_msg',decoded_msg)
    return decoded_msg

def get_reflected(idx,reflector):
    tup = [nbr for nbr in reflector if idx in nbr][0]
    if tup[0] == idx:
        return tup[1]
    else:
        return tup[0]

# Enigma


reflector = REFLECTORS[1]["value"]

def code_letter(letter):
    idx = ord(letter)-65 
    enc = rotor_a.encode_idx(idx)
    enc = rotor_b.encode_idx(enc)
    enc = rotor_c.encode_idx(enc)
    enc = get_reflected(enc,reflector)
    enc = rotor_c.decode_idx (enc)
    enc = rotor_b.decode_idx (enc)
    enc = rotor_a.decode_idx (enc)
    return chr(enc+65)



def code_word(word,rotor_idxs):
    reset_rotors(rotor_idxs)
    result = ""
    for letter in word.upper().replace(' ','Z'):
        result += code_letter(letter)
        rotate_rotors()

    return result



word = "the quick brown fox jumps over the lazy dog"
rotor_idxs   = [0,0,0]
# rotor_idxs = [1,1,1]
# rotor_idxs = [3,2,25]

res = code_word(word,rotor_idxs)
print(res,code_word(res,rotor_idxs))
# print(code_word(word,rotor_idxs),code_word(code_word(word,rotor_idxs),rotor_idxs))


print('done')



