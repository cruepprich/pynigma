tup = ('A','B')
tl = [tup,('B','C')]

x = next(x for x in tl if x[0] == 'B')
print(x)

x = (x for x in tl)
print(next(x))
print(next(x))

import random as rnd
rndlist = list(map(lambda x: chr(x), rnd.sample(range(65,91),26)))

# x =[pair for pair in enumerate(rndlist)]
# Generate rotor tuples
# x = list((chr(idx + 65),value) for idx, value in enumerate(rndlist))
# print(x,len(x),type(x))

# nbr_tuple_list = [(idx,val) for idx, val in enumerate(list(rnd.sample(range(26),26)))]
# print(nbr_tuple_list)


# refa = list(map(lambda x: chr(x), rnd.sample(range(65,78),13)))
# refb = list(map(lambda x: chr(x), rnd.sample(range(78,91),13)))
# print(refa)
# print(refb)
# ref = zip(refa,refb)
ref = zip(list(map(lambda x: chr(x), rnd.sample(range(65,78),13)))
         ,list(map(lambda x: chr(x), rnd.sample(range(78,91),13))))
print(list(ref))

print('result',[letter for letter in ref])

a = [(1,2),(1,4),(3,5),(5,7)]
a = [('a','b'),('c','d')]
a = [('M', 'O'), ('G', 'T'), ('B', 'N'), ('H', 'Q'), ('C', 'Z'), ('A', 'S'), ('K', 'X'), ('I', 'U'), ('D', 'V'), ('L', 'R'), ('J', 'Y'), ('F', 'P'), ('E', 'W')]

print("res",[item for item in a if 'O' in item])
