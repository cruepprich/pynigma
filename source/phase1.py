'''
Phase 1
Parse command line options. 
Do a basic en/decryption of three letters
'''
import sys,getopt,os

alphabet = "ABC"
cipher   = "CAB"

usage_help = ('%s -m [encode|decode] text' % os.path.basename(__file__))

def encode(text):
    alphabet_index = alphabet.index(text)
    new_letter     = cipher[alphabet_index]
    return new_letter

def decode(text):
    cipher_index = cipher.index(text)
    new_letter   = alphabet[cipher_index]
    return new_letter

# Main function should always be toward the bottom of the code
def main(argv):
    mode       = "" #assiged via command line option

    # Get command line options
    try:
        # Assign command options and arguments to opts and args respectively
        opts, args = getopt.getopt(argv,"hm:")
    except getopt.GetoptError:
        # the option is not -h or -m, raise exception
        print("Invalid options")
        print(usage_help)
        sys.exit(2)


    # Loop through command line options. If -m is found grab its argument
    for opt,arg in opts:
        if opt == "-m":
            mode = arg
            # Verify that the mode is either encode or decode
            if mode not in ('encode','decode'):
                print(usage_help)
                sys.exit(2)
        elif opt == '-h':
            print('help!!!')
            sys.exit(0)
    
    # Check if a text was passed on the command line
    try:
        text = args[0]
    except IndexError:
        print('No text was given')
        sys.exit(2)

    print('Mode: ',mode)
    print('text: ',args[0])

    if mode == "encode":
        print("Encoded result",encode(text))
    elif mode == "decode":
        print("Decoded result",decode(text))
    else:
        print("Invalid mode ",mode)
        sys.exit(2)

    


# Good Python practice:
if __name__ == "__main__":
    main(sys.argv[1:])
